import Vue from 'vue'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router'

Vue.use(Vuelidate)
Vue.config.productionTip = false

// Register a global custom directive called `v-focus`
Vue.directive('focus', {
    inserted: function(el) {
        el.focus()
    }
})

Vue.filter('tomoney', function(value) {
    if (value == null || !value.toString()) return ''

	let monto = parseFloat(value).toFixed(2);
    var separado = monto.toString().split('.');
    var entero = (separado[0]).split('');
    let formatted = '';
    for (let i = 0; i < entero.length; i++) {
        const element = entero[entero.length - 1 - i];
        if (i % 3 == 0 && i != 0) {
            formatted = ',' + formatted;
        }
        formatted = element + formatted;
    }
    if (formatted.toString().length > 0) {
        formatted = 'S/. ' + formatted;
    }
    if (separado[1]) {
        formatted = formatted + '.' + separado[1];
    }
    return formatted;
})

Vue.filter('todate', function (value) {
	if (value == null || !value.toString()) return ''

    var date = value.toString();
    return "".concat(date.substr(8, 2), '/', date.substr(5, 2), '/', date.substr(0, 4));
})

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')