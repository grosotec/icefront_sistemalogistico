import Vue from 'vue'
import Router from 'vue-router'
//import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Dashboard from './views/Dashboard.vue'

import Clientes from './views/clientes/List.vue'
import Clientes_create from './views/clientes/Create.vue'
import Clientes_show from './views/clientes/Show.vue'
import Clientes_edit from './views/clientes/Edit.vue'

import Personal from './views/personal/List.vue'
import Personal_create from './views/personal/Create.vue'
import Personal_show from './views/personal/Show.vue'
import Personal_edit from './views/personal/Edit.vue'

import Usuarios from './views/usuarios/List.vue'
import Usuarios_show from './views/usuarios/Show.vue'
import Usuarios_create from './views/usuarios/Create.vue'
import Usuarios_edit from './views/usuarios/Edit.vue'

import Descuentos from './views/descuentos/List.vue'
import Descuentos_create from './views/descuentos/Create.vue'


import Adelantos from './views/adelantos/List.vue'
import Adelantos_create from './views/adelantos/Create.vue'

import Pagos from './views/pagos/List.vue'
import Pagos_show from './views/pagos/Show.vue'
import Pagos_create from './views/pagos/Create.vue'

import Almacenes from './views/almacenes/List.vue'
import Almacenes_create from './views/almacenes/Create.vue'
import Almacenes_edit from './views/almacenes/Edit.vue'

import Adelantos_clientes from './views/adelantos_clientes/List.vue'
import Adelantos_clientes_create from './views/adelantos_clientes/Create.vue'

import Proveedores from './views/proveedores/List.vue'
import Proveedores_edit from './views/proveedores/Edit.vue'

import Certificaciones from './views/certificaciones/List.vue'
import Certificaciones_show from './views/certificaciones/Show.vue'
import Certificaciones_create from './views/certificaciones/Create.vue'
import Certificaciones_edit from './views/certificaciones/Edit.vue'

import Productos from './views/productos/List.vue'
import Productos_show from './views/productos/Show.vue'
import Productos_create from './views/productos/Create.vue'
import Productos_edit from './views/productos/Edit.vue'

import Catalogo from './views/productos/Catalogo.vue'

import Reporte_entradas from './views/reporte_entradas/List.vue'

import Reporte_salidas from './views/reporte_salidas/List.vue'

import Kardex from './views/kardex/List.vue'

import Entradas from './views/entradas/List.vue'
import Entradas_show from './views/entradas/Show.vue'
import Entradas_create from './views/entradas/Create.vue'
import Entradas_edit from './views/entradas/Edit.vue'

import Salidas from './views/salidas/List.vue'
import Salidas_show from './views/salidas/Show.vue'
import Salidas_create from './views/salidas/Create.vue'
import Salidas_edit from './views/salidas/Edit.vue'

Vue.use(Router)

export default new Router({
    //mode: 'history',
    routes: [
        //{
        //    path: '/',
        //    redirect: '/login'
        //},
        //{
        //path: '/about',
        //name: 'about',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        //component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        //},
        //{
        //    path: '/',
        //    name: 'login',
            //component: () => import( './views/Login.vue')
        //    component: Login
        //},
        {
            path: '/dashboard',
            name: 'dashboard',
            component: Dashboard
        },
        //Clientes
        {
            path: '/mant_clientes',
            name: 'mant_clientes',
            component: Clientes,
            props: true
        },
        {
            path: '/mant_clientes/ver/:id',
            name: 'show_clientes',
            component: Clientes_show
        },
        {
            path: '/mant_clientes/insertar',
            name: 'create_clientes',
            component: Clientes_create
        },
        {
            path: '/mant_clientes/editar/:id',
            name: 'edit_clientes',
            component: Clientes_edit
        },
        //Personal
        {
            path: '/mant_personal',
            name: 'mant_personal',
            component: Personal,
            props: true
        },
        {
            path: '/mant_personal/ver/:id',
            name: 'show_personal',
            component: Personal_show
        },
        {
            path: '/mant_personal/insertar',
            name: 'create_personal',
            component: Personal_create
        },
        {
            path: '/mant_personal/editar/:id',
            name: 'edit_personal',
            component: Personal_edit
        },
        //Users
        {
            path: '/usuarios',
            name: 'usuarios',
            component: Usuarios,
            props: true
        },
        {
            path: '/usuarios/ver/:id',
            name: 'show_usuarios',
            component: Usuarios_show
        },
        {
            path: '/usuarios/insertar',
            name: 'create_usuarios',
            component: Usuarios_create
        },
        {
            path: '/usuarios/editar/:id',
            name: 'edit_usuarios',
            component: Usuarios_edit
        },
        //Descuentos
        {
            path: '/descuentos',
            name: 'descuentos',
            component: Descuentos,
            props: true
        },
        {
            path: '/descuentos/insertar',
            name: 'create_descuentos',
            component: Descuentos_create
        },

        //Adelantos
        {
            path: '/adelantos',
            name: 'adelantos',
            component: Adelantos,
            props: true
        },
        {
            path: '/adelantos/insertar',
            name: 'create_adelantos',
            component: Adelantos_create
        },
        //Pagos
        {
            path: '/pagos',
            name: 'pagos',
            component: Pagos,
            props: true
        },
        {
            path: '/pagos/ver/:id',
            name: 'show_pagos',
            component: Pagos_show
        },
        {
            path: '/pagos/insertar',
            name: 'create_pagos',
            component: Pagos_create
        },
        //Almacenes
        {
            path: '/almacenes',
            name: 'almacenes',
            component: Almacenes,
            props: true
        },
        {
            path: '/almacenes/insertar',
            name: 'create_almacenes',
            component: Almacenes_create
        },
        {
            path: '/almacenes/editar/:id',
            name: 'edit_almacenes',
            component: Almacenes_edit
        },

        //Adelantos_clientes
        {
            path: '/adelantos_proveedores',
            name: 'adelantos_proveedores',
            component: Adelantos_clientes,
            props: true
        },
        {
            path: '/adelantos_proveedores/insertar',
            name: 'create_adelantos_proveedores',
            component: Adelantos_clientes_create
        },
        //Proveedores
        {
            path: '/proveedores',
            name: 'proveedores',
            component: Proveedores,
            props: true
        },
        {
            path: '/proveedores/editar/:id',
            name: 'edit_proveedores',
            component: Proveedores_edit
        },
        //Certificaciones
        {
            path: '/certificaciones',
            name: 'certificaciones',
            component: Certificaciones,
            props: true
        },
        {
            path: '/certificaciones/editar/:id',
            name: 'edit_certificaciones',
            component: Certificaciones_edit
        },
        {
            path: '/certificaciones/ver/:id',
            name: 'show_certificaciones',
            component: Certificaciones_show
        },
        {
            path: '/certificaciones/insertar',
            name: 'create_certificaciones',
            component: Certificaciones_create
        },
        //Productos
        {
            path: '/productos',
            name: 'productos',
            component: Productos,
            props: true
        },
        {
            path: '/productos/editar/:id',
            name: 'edit_productos',
            component: Productos_edit
        },
        {
            path: '/productos/ver/:id',
            name: 'show_productos',
            component: Productos_show
        },
        {
            path: '/productos/insertar',
            name: 'create_productos',
            component: Productos_create
        },
        //Catalogo
        {
            path: '/catalogo',
            name: 'catalogo',
            component: Catalogo
        },
        //reporte_entradas
        {
            path: '/reporte_entradas',
            name: 'reporte_entradas',
            component: Reporte_entradas,
            props: true
        },

        //reporte_salidas
        {
            path: '/reporte_salidas',
            name: 'reporte_salidas',
            component: Reporte_salidas,
            props: true
        },
        //kardex
        {
            path: '/kardex',
            name: 'kardex',
            component: Kardex,
            props: true
        },
        //Entradas
        {
            path: '/entradas',
            name: 'entradas',
            component: Entradas,
            props: true
        },
        {
            path: '/entradas/ver/:id',
            name: 'show_entradas',
            component: Entradas_show
        },

        {
            path: '/entradas/insertar',
            name: 'create_entradas',
            component: Entradas_create
        },
        {
            path: '/entradas/editar/:id',
            name: 'edit_entradas',
            component: Entradas_edit
        },
        //Salidas
        {
            path: '/salidas',
            name: 'salidas',
            component: Salidas,
            props: true
        },
        {
            path: '/salidas/ver/:id',
            name: 'show_salidas',
            component: Salidas_show
        },

        {
            path: '/salidas/insertar',
            name: 'create_salidas',
            component: Salidas_create
        },
        {
            path: '/salidas/editar/:id',
            name: 'edit_salidas',
            component: Salidas_edit
        },
    ]
});