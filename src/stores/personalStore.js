import axios from 'axios';
import { server } from '../api/server';

export const personalStore = {
    state: {
        selected: '',
        personals: []
    },
    getPersonal() {
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host + '/RRHH/Personal')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        personalStore.state.personals = [];
                        response.data.data.forEach(element => {
                            personalStore.state.personals.push(element);
                        });
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return promise;
    },
    save(idcliente, idlocal, idcargo, idestado, dfechainilabores,
        nmontosueldo, lplanilla, idusureg) {
        var savePromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/RRHH/Personal/insert', {
                    'idcliente': idcliente,
                    'idlocal': idlocal,
                    'idcargo': idcargo,
                    'idestado': idestado,
                    'dfechainilabores': dfechainilabores,
                    'nmontosueldo': nmontosueldo,
                    'lplanilla': lplanilla,
                    'idusureg': idusureg
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Personal guardado con exito");
                        personalStore.getPersonal();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el guardaddo");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return savePromise;
    },
    edit(idcliente, idlocal, idcargo, idestado, dfechainilabores,
        nmontosueldo, lplanilla, idusumod, idpersonal) {
        var editPromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/RRHH/Personal/update', {
                    'idcliente': idcliente,
                    'idlocal': idlocal,
                    'idcargo': idcargo,
                    'idestado': idestado,
                    'dfechainilabores': dfechainilabores,
                    'nmontosueldo': nmontosueldo,
                    'lplanilla': lplanilla,
                    'idusumod': idusumod,
                    'idpersonal': idpersonal
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Cliente actualizado con exito");
                        personalStore.getPersonal();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el actualizado");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return editPromise;
    },
    baja(idpersonal, idusumod) {
        var editPromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/RRHH/Personal/baja', {
                    'idpersonal': idpersonal,
                    'idusumod': idusumod
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Personal bajado con exito");
                        personalStore.getPersonal();
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return editPromise;
    },
    cesar(idpersonal, idusumod, dfechafinlabores) {
        var editPromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/RRHH/Personal/cesar', {
                    'idpersonal': idpersonal,
                    'idusumod': idusumod,
                    'dfechafinlabores': dfechafinlabores
                })
                //axios.get(server.host+'/RRHH/Personal/cesar?idpersonal='+idpersonal+'&idusumod='+idusumod+'&dfechafinlabores='+dfechafinlabores)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Personal cesado con exito");
                        personalStore.getPersonal();
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return editPromise;
    }
};