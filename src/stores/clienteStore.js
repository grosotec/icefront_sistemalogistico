import axios from 'axios';
import { server } from '../api/server';

export const clienteStore = {
    state: {
        selected: '',
        clientes : []
    },
    getClientes() {
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host+'/RRHH/Clientes')
            .then(response => {
                // JSON responses are automatically parsed.
                if(response.data.data != null){
                    clienteStore.state.clientes = [];
                    response.data.data.forEach(element => {
                        clienteStore.state.clientes.push(element);
                    });
                    resolve(response.data);
                } else {
                    reject(response.data.message);
                }
            })
            .catch(e => {
                reject("Error al conectarse con el servidor");
            })                
        })
        return promise;
    },
    save(idtipodoc, cnumdoc, idtipopersona, cnombpersona, dfechanacimiento,
         cnumcel, idusureg, ccoddpto, ccodprov, ccoddist) {
             var savePromise = new Promise((resolve, reject) => {
                 axios.post(server.host+'/RRHH/Clientes/insert', {
                     'idtipodoc' : idtipodoc,
                     'cnumdoc' : cnumdoc,
                     'idtipopersona' : idtipopersona,
                     'cnombpersona' : cnombpersona,
                     'dfechanacimiento' : dfechanacimiento,
                     'cnumcel' : cnumcel,
                     'idusureg' : idusureg,
                     'ccoddpto' : ccoddpto,
                     'ccodprov' : ccodprov,
                     'ccoddist' : ccoddist
                    })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Cliente guardado con exito");
                        clienteStore.getClientes();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el guardaddo");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: "+e);
                })
            })
            return savePromise;
    },
    edit(idcliente, idtipodoc, cnumdoc, idtipopersona, cnombpersona, dfechanacimiento,
         cnumcel, idusureg, ccoddpto, ccodprov, ccoddist) {
             var editPromise = new Promise((resolve, reject) => {
                 axios.post(server.host+'/RRHH/Clientes/update', {
                     'idcliente' : idcliente,
                     'idtipodoc' : idtipodoc,
                     'cnumdoc' : cnumdoc,
                     'idtipopersona' : idtipopersona,
                     'cnombpersona' : cnombpersona,
                     'dfechanacimiento' : dfechanacimiento,
                     'cnumcel' : cnumcel,
                     'idusumod' : idusureg,
                     'ccoddpto' : ccoddpto,
                     'ccodprov' : ccodprov,
                     'ccoddist' : ccoddist
                    })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Cliente actualizado con exito");
                        clienteStore.getClientes();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el actualizado");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: "+e);
                })
            })
            return editPromise;
    },
    baja(idcliente) {
        var editPromise = new Promise((resolve, reject) => {
            axios.get(server.host+'/RRHH/Clientes/baja?idcliente='+idcliente)
           .then(response => {
               // JSON responses are automatically parsed.
               if (response.data.success == true) {
                   console.log("Cliente bajado con exito");
                   clienteStore.getClientes();
                   resolve(response.data);
               } else {
                   reject(response.data.message);
               }
           })
           .catch(e => {
               reject("Error al conectarse con el servidor");
               console.log("Error: "+e);
           })
       })
       return editPromise;
    },
    validarDoc(cnumdoc){
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host+'/GEN/validar?cnumdoc='+cnumdoc)
           .then(response => {
                // JSON responses are automatically parsed.
                if (response.data.success == true) {
                    resolve(response.data.data[0].existencia);
                } else {
                    reject("Error al intentar validar dni");
                }
           })
           .catch(e => {
               reject("Error al conectarse con el servidor");
               console.log("Error: "+e);
           })
       })
       return promise;
    }
};