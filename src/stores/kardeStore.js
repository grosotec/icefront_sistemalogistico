import axios from 'axios';
import { server } from '../api/server';

export const kardeStore = {
    state: {
        selected: '',
        kardex: [],
        uri: ''
    },
    getKarde(idproducto, fecha_inicio, fecha_final) {
        var promise = new Promise((resolve, reject) => {
            //axios.get(server.host + '/LOG/KDetalleProductos?idproducto=' + idproducto + '&idalmacen=' + idalmacen + '&fechainicio=' + fecha_inicio + '&fechafinal=' + fecha_final)

            axios.get(server.host + '/LOG/KDetalleProductos?idproducto=' + idproducto + '&fechainicio=' + fecha_inicio + '&fechafinal=' + fecha_final)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        kardeStore.state.kardex = [];
                        response.data.data.forEach(element => {
                            kardeStore.state.kardex.push(element);
                        });
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return promise;
    },
    getReporte(idproducto, fecha_inicio, fecha_final) {
        var promise = new Promise(() => {
            // this.uri =  server.host + "/Reporte/Entradas?fechaini=" + this.reformatDateString(fecha_inicio) + "&fechafin=" + this.reformatDateString(fecha_final) + "&idalmacen=" + idalmacen;


            axios.get(server.host + '/Reporte/Kardex?fechaini=' + fecha_inicio + '&fechafin=' + fecha_final + '&idproducto=' + idproducto);

            kardeStore.state.uri = server.host + '/Reporte/Kardex?fechaini=' + fecha_inicio + '&fechafin=' + fecha_final + '&idproducto=' + idproducto;


        });
        return promise;
    },




};