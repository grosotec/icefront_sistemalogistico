import axios from 'axios';
import { server } from '../api/server';

export const proveedorStore = {
    state: {
        selected: '',
        proveedores: [],
        proveedorMontos: [],
        datos: []
    },
    getProveedores() {
        var usersPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/RRHH/Clientes')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        proveedorStore.state.proveedores = [];
                        response.data.data.forEach(element => {
                            proveedorStore.state.proveedores.push(element);
                        });
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor!!!");
                })
        })
        return usersPromise;
    },

    getProveedorMonto(idmantproveedor) {
        var usersPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/LOG/ListProveedor?idmantproveedor=' + idmantproveedor)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        proveedorStore.state.proveedorMontos = [];
                        response.data.data.forEach(element => {
                            proveedorStore.state.proveedorMontos.push(element);
                        });
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor!!!");
                })
        })
        return usersPromise;
    },

    edit(id_cliente, n_montomaximo, id_usureg) {

        var editPromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/LOG/EvaluarProveedor?id_cliente=' + id_cliente + '&n_montomaximo=' + n_montomaximo + '&id_usureg=' + id_usureg)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Monto asignado  con exito");
                        proveedorStore.getProveedores();
                        if (response.data.data != null) {
                            proveedorStore.state.datos = [];
                            response.data.data.forEach(element => {
                                proveedorStore.state.datos.push(element);
                            });
                            resolve(response.data);
                        } else {
                            reject(response.data.message);
                        }
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar la asignacion");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return editPromise;
    },


};