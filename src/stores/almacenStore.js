import axios from 'axios';
import { server } from '../api/server';

export const almacenStore = {
    state: {
        selected: '',
        almacenes: []
    },
    getAlmacenes() {
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host + '/LOG/Almacen')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        almacenStore.state.almacenes = [];
                        response.data.data.forEach(element => {
                            almacenStore.state.almacenes.push(element);
                        });
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return promise;
    },
    save(cnombrealmacen, ccoddpto, ccodprov, ccoddist, ccodalmacen, cnumtelefono,
        idlocal, idpersonal, idusureg) {
        var savePromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/LOG/Almacen/insert', {
                    'cnombrealmacen': cnombrealmacen,
                    'ccoddpto': ccoddpto,
                    'ccodprov': ccodprov,
                    'ccoddist': ccoddist,
                    'ccodalmacen': ccodalmacen,
                    'cnumtelefono': cnumtelefono,
                    'idlocal': idlocal,
                    'idpersonal': idpersonal,
                    'idusureg': idusureg

                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Almacen guardado con exito");
                        almacenStore.getAlmacenes();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el guardaddo");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return savePromise;
    },
    edit(idalmacen, cnombrealmacen, idpersonal, cnumtelefono, idusumod) {
        var editPromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/LOG/Almacen/update', {
                    'idalmacen': idalmacen,
                    'cnombrealmacen': cnombrealmacen,
                    'idpersonal': idpersonal,
                    'cnumtelefono': cnumtelefono,
                    'idusumod': idusumod

                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Almacen actualizado con exito");
                        almacenStore.getAlmacenes();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el actualizado");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return editPromise;
    },
    baja(idalmacen) {
        var bajaPromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/LOG/Almacen/update', {
                    'idalmacen': idalmacen
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Almacen bajado con exito");
                        almacenStore.getAlmacenes();
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return bajaPromise;
    },

};