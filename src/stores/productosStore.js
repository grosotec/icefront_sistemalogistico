import axios from 'axios';
import { server } from '../api/server';

export const productosStore = {
    state: {
        selected: '',
        productos: [],
        carro: []
    },
    getProductos() {
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host+'/LOG/Productos')
            .then(response => {
                // JSON responses are automatically parsed.
                if(response.data.data != null){
                    productosStore.state.productos = [];
                    response.data.data.forEach(element => {
                        productosStore.state.productos.push(element);
                    });
                    resolve(response.data);
                } else {
                    reject(response.data.message);
                }
            })
            .catch(e => {
                reject("Error al conectarse con el servidor");
            })                
        })
        return promise;
    },
    save(ccodproducto, cnombproducto, cdescproducto, 
        cdetalleproducto, idtipo, idcertificado,
        idunidmedida, idusureg) {
             var savePromise = new Promise((resolve, reject) => {
                 axios.post(server.host+'/LOG/Productos/insert', {
                    'ccodproducto' : ccodproducto,
                    'cnombproducto' : cnombproducto,
                    'cdescproducto' : cdescproducto,
                    'cdetalleproducto' : cdetalleproducto,
                    'idtipo' : idtipo,
                    'idcertificado' : idcertificado,
                    'idunidmedida' : idunidmedida,
                    'idusureg' : idusureg
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Producto guardado con exito");
                        productosStore.getProductos();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el guardado");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: "+e);
                })
            })
            return savePromise;
    },
    edit(ccodproducto, cnombproducto, cdescproducto, 
        cdetalleproducto, idtipo, idcertificado,
        idunidmedida, idusumod, idproducto) {
             var editPromise = new Promise((resolve, reject) => {
                 axios.post(server.host+'/LOG/Productos/update', {
                    'ccodproducto' : ccodproducto,
                    'cnombproducto' : cnombproducto,
                    'cdescproducto' : cdescproducto,
                    'cdetalleproducto' : cdetalleproducto,
                    'idtipo' : idtipo,
                    'idcertificado' : idcertificado,
                    'idunidmedida' : idunidmedida,
                    'idusumod' : idusumod,
                    'idproducto' : idproducto
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Producto actualizado con exito");
                        productosStore.getProductos();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el actualizado");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: "+e);
                })
            })
            return editPromise;
    },
    baja(idproducto, idusumod) {
        var editPromise = new Promise((resolve, reject) => {
            axios.post(server.host+'/LOG/Productos/baja', {
                'idproducto': idproducto,
                'idusumod': idusumod
            })
           .then(response => {
               // JSON responses are automatically parsed.
               if (response.data.success == true) {
                   console.log("Personal bajado con exito");
                   productosStore.getProductos();
                   resolve(response.data);
               } else {
                   reject(response.data.message);
               }
           })
           .catch(e => {
               reject("Error al conectarse con el servidor");
               console.log("Error: "+e);
           })
       })
       return editPromise;
    },
    getProductosVenta() {
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host+'/LOG/ListarProductos')
            .then(response => {
                // JSON responses are automatically parsed.
                if(response.data.data != null){
                    resolve(response.data);
                } else {
                    reject("Error al obtener productos");
                }
            })
            .catch(e => {
                reject("Error al conectarse con el servidor");
            })                
        })
        return promise;
    },
};