import axios from 'axios';
import { server } from '../api/server';

export const generalStore = {
    state: {
        productos: []

    },
    getProductos() {
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host + '/LOG/Productos')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        generalStore.state.productos = [];
                        response.data.data.forEach(element => {
                            generalStore.state.productos.push(element);
                        });
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return promise;
    },
    getDepartamentos() {
        var ubigeoPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/GEN/listar/departamentos')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return ubigeoPromise;
    },
    getProvincias(id) {
        var ubigeoPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/GEN/listar/provincias?ccoddpto=' + id)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return ubigeoPromise;
    },
    getDistritos(id) {
        var ubigeoPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/GEN/listar/distritos?ccodprov=' + id)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return ubigeoPromise;
    },
    getLocales() {
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host + '/GEN/listar/locales')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return promise;
    },
    getCargos() {
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host + '/GEN/listar/cargos')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return promise;
    },
    getCodigo(id) {
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host + '/GEN/generar/codigo?idlocal=' + id)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {

                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return promise;
    },
    getCertificacion() {
        var certificacionPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/LOG/Certificados')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })

        return certificacionPromise;
    },
    getTipos() {
        var tipoPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/GEN/listar/tipoproductos')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return tipoPromise;
    },
    getAlmacenes() {
        var certificacionPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/LOG/Almacen')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })

        return certificacionPromise;
    },
    getUnidads() {
        var certificacionPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/GEN/listar/unidmedida')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return certificacionPromise;
    },
    getLocales() {
        var certificacionPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/GEN/listar/locales')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return certificacionPromise;
    },
    getConceptos() {
        var conceptoPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/GEN/listar/conceptos')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return conceptoPromise;
    },
    changeFormat(date) {

        return "".concat(date.substr(8, 2), '/', date.substr(5, 2), '/', date.substr(0, 4));
    }
};