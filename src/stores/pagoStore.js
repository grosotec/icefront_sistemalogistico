import axios from 'axios';
import { server } from '../api/server';

export const pagoStore = {
    state: {
        selected: '',
        pagos: []
    },
    getPagoNow() {
        var pagoPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/RRHH/CPago')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })

        return pagoPromise;
    },
    getPagoDate(id) {
        var pagoPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/RRHH/DPagos?idpago='+id)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })

        return pagoPromise;
    },
    getPagoList() {
        var pagoPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/RRHH/ListarPagos')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        response.data.data.forEach(element => {
                            pagoStore.state.pagos.push(element);
                        });
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })

        return pagoPromise;
    },
    pagar(idusureg) {
        var savePromise = new Promise((resolve, reject) => {
            axios.get(server.host+'/RRHH/HPagos?idusureg='+idusureg)
            .then(response => {
                // JSON responses are automatically parsed.
                if (response.data.success == true) {
                    console.log("Pago realizado con exito");
                    resolve(response.data);
                } else {
                    //reject(response.data.message);
                    reject("Error al procesar el guardado");
                }
            })
            .catch(e => {
                reject("Error al conectarse con el servidor");
                console.log("Error: "+e);
            })
       })
       return savePromise;
    }
};