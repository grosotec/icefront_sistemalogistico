import axios from 'axios';
import { server } from '../api/server';

export const certificacionStore = {
    state: {
        selected: '',
        certificaciones: []
    },
    getCertificaciones() {
        var usersPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/LOG/Certificacion')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        certificacionStore.state.certificaciones = [];
                        response.data.data.forEach(element => {
                            certificacionStore.state.certificaciones.push(element);
                        });
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor!!!");
                })
        })
        return usersPromise;
    },
    save(idcertificado, idcliente, topekilos, idusureg) {
        var savePromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/LOG/Certificacion/insert', {
                    'idcertificado': idcertificado,
                    'idcliente': idcliente,
                    'topekilos': topekilos,
                    'idusureg': idusureg
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Certificacion guardado con exito");
                        certificacionStore.getCertificaciones();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el guardaddo");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return savePromise;
    },
    edit(idcertproveedor, idcertificado, idcliente, topekilos, idusureg) {
        var editPromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/LOG/Certificacion/update', {
                    'idcertproveedor': idcertproveedor,
                    'idcertificado': idcertificado,
                    'idcliente': idcliente,
                    'topekilos': topekilos,
                    'idusureg': idusureg
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Certificacion actualizada con exito");
                        certificacionStore.getCertificaciones();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el actualizado");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return editPromise;
    },


};