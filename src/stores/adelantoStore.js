import axios from 'axios';
import { server } from '../api/server';

export const adelantoStore = {
    state: {
        selected: '',
        adelantos: []
    },
    getAdelantos() {
        var descPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/RRHH/Adelantos')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        adelantoStore.state.adelantos = [];
                        response.data.data.forEach(element => {
                            adelantoStore.state.adelantos.push(element);
                        });
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor!!!");
                })
        })
        return descPromise;
    },
    save(idpersonal, cconcepto, cdetalle, nmonto, nmontosueldo, idestado, idusureg) {
        var savePromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/RRHH/Adelantos/insert', {
                    'idpersonal': idpersonal,
                    'cconcepto': cconcepto,
                    'cdetalle': cdetalle,
                    'nmonto': nmonto,
                    'nmontosueldo': nmontosueldo,
                    'idestado': idestado,
                    'idusureg': idusureg
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Adelanto guardado con exito");
                        adelantoStore.getAdelantos();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el guardaddo");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return savePromise;
    },



};