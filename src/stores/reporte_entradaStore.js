import axios from 'axios';
import { server } from '../api/server';

export const reporte_entradaStore = {
    state: {
        selected: '',
        reporte_entradas: [],
        uri: ''
    },
    getReporte_entrada(fecha_inicio, fecha_final, idalmacen) {
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host + '/LOG/KEntradaProductos?fechaini=' + fecha_inicio + '&fechafin=' + fecha_final + '&idalmacen=' + idalmacen)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        reporte_entradaStore.state.reporte_entradas = [];
                        response.data.data.forEach(element => {
                            reporte_entradaStore.state.reporte_entradas.push(element);
                        });
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return promise;
    },

    getReporte(fecha_inicio, fecha_final, idalmacen) {
        var promise = new Promise(() => {
            // this.uri =  server.host + "/Reporte/Entradas?fechaini=" + this.reformatDateString(fecha_inicio) + "&fechafin=" + this.reformatDateString(fecha_final) + "&idalmacen=" + idalmacen;


            axios.get(server.host + '/Reporte/Entradas?fechaini=' + fecha_inicio + '&fechafin=' + fecha_final + '&idalmacen=' + idalmacen);

            reporte_entradaStore.state.uri = server.host + '/Reporte/Entradas?fechaini=' + fecha_inicio + '&fechafin=' + fecha_final + '&idalmacen=' + idalmacen;


        });
        return promise;
    },




};