import axios from 'axios';
import { server } from '../api/server';

export const adelantos_clienteStore = {
    state: {
        selected: '',
        adelantos_clientes: []
    },
    getAdelantos_clientes() {
        var descPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/LOG/Adelantos')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        adelantos_clienteStore.state.adelantos_clientes = [];
                        response.data.data.forEach(element => {
                            adelantos_clienteStore.state.adelantos_clientes.push(element);
                        });
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor!!!");
                })
        })
        return descPromise;
    },
    save(idcliente, cconcepto, cdetalle, nmonto, idusureg) {
        var savePromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/LOG/Adelantos/insert', {
                    'idcliente': idcliente,
                    'cconcepto': cconcepto,
                    'cdetalle': cdetalle,
                    'nmonto': nmonto,
                    'idusureg': idusureg
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Adelanto guardado con exito");
                        adelantos_clienteStore.getAdelantos_clientes();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el guardaddo");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return savePromise;
    },



};