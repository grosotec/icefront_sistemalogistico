import axios from 'axios';
import { server } from '../api/server';

export const entradaStore = {
    state: {
        selected: '',
        entradas: []
    },
    getEntradas() {
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host + '/LOG/EntradaProductos')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        entradaStore.state.entradas = [];
                        response.data.data.forEach(element => {
                            entradaStore.state.entradas.push(element);
                        });
                        resolve(response.data);
                    } else {
                        reject("Error al obtener entradas");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return promise;
    },

    save(idalmacen, idcliente, idproducto,
        npreciobase, nprecioventa, npesobruto, npesoneto, ntara,
        ccoddpto, ccodprov, ccoddist, idusureg) {
        var savePromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/LOG/EntradaProductos/insert', {
                    'idalmacen': idalmacen,
                    'idcliente': idcliente,
                    'idproducto': idproducto,
                    'npreciobase': npreciobase,
                    'nprecioventa': nprecioventa,
                    'npesobruto': npesobruto,
                    'npesoneto': npesoneto,
                    'ntara': ntara,
                    'ccoddpto': ccoddpto,
                    'ccodprov': ccodprov,
                    'ccoddist': ccoddist,
                    'idusureg': idusureg
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Producto guardado con exito");
                        entradaStore.getEntradas();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el guardado");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return savePromise;
    },

    edit(identradaproducto, idalmacen, idcliente, idproducto,
        npreciobase, nprecioventa, npesobruto, npesoneto, ntara, ccoddpto, ccodprov, ccoddist, idusureg) {
        var editPromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/LOG/EntradaProductos/update', {
                    'identradaproducto': identradaproducto,
                    'idalmacen': idalmacen,
                    'idcliente': idcliente,
                    'idproducto': idproducto,
                    'npreciobase': npreciobase,
                    'nprecioventa': nprecioventa,
                    'npesobruto': npesobruto,
                    'npesoneto': npesoneto,
                    'ntara': ntara,
                    'ccoddpto': ccoddpto,
                    'ccodprov': ccodprov,
                    'ccoddist': ccoddist,
                    'idusureg': idusureg
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Entrada actualizada con exito");
                        entradaStore.getEntradas();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el actualizado");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return editPromise;
    },
    baja(identradaproducto, idusumod) {
        var editPromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/LOG/EntradaProductos/baja', {
                    'identradaproducto': identradaproducto,
                    'idusumod': idusumod
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Personal bajado con exito");
                        entradaStore.getEntradas();
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return editPromise;
    }

};