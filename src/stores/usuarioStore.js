import axios from 'axios';
import { server } from '../api/server';

export const usuarioStore = {
    state: {
        selected: '',
        usuarios: []
    },
    getUsuarios() {
        var usersPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/RRHH/Usuario')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        usuarioStore.state.usuarios = [];
                        response.data.data.forEach(element => {
                            usuarioStore.state.usuarios.push(element);
                        });
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor!!!");
                })
        })
        return usersPromise;
    },
    save(idpersonal, cusuario, cpassword, idperfil, idusureg, idusumod, lvigente) {
        var savePromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/RRHH/Usuario/insert', {
                    'idpersonal': idpersonal,
                    'cusuario': cusuario,
                    'cpassword': cpassword,
                    'idperfil': idperfil,
                    'idusureg': idusureg,
                    'idusumod': idusumod,
                    'lvigente': lvigente
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Usuario guardado con exito");
                        usuarioStore.getUsuarios();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el guardaddo");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return savePromise;
    },
    edit(idusuario, cpassword) {
        var editPromise = new Promise((resolve, reject) => {
            axios.post(server.host + '/RRHH/Usuario/update', {
                    'idusuario': idusuario,
                    'cpassword': cpassword
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Contraseña restablecida con exito");
                        usuarioStore.getUsuarios();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el actualizado");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return editPromise;
    },
    baja(idusuario) {
        var bajaPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/RRHH/Usuario/baja?idusuario=' + idusuario)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Usuario bajado con exito");
                        usuarioStore.getUsuarios();
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: " + e);
                })
        })
        return bajaPromise;
    }

};