import axios from 'axios';
import { server } from '../api/server';

export const userStore = {
    state: {
        user: '',
        loading: false,
        authenticated: false,
        menu: []
    },
    login(username, password){
        var loginPromise = new Promise((resolve, reject) => {
            axios.get(server.host+'/SEG/usuario/login?cUsuario='+username+'&cPassword='+password)
            .then(response => {
                // JSON responses are automatically parsed.
                if(response.data.data != null){
                    userStore.state.user = response.data.data;
                    userStore.getMenu(userStore.state.user.idPerfil).then( () => {
                        resolve(response.data);
                    })
                } else {
                    reject(response.data.message);
                }
            })
            .catch(e => {
                reject("Error al conectarse con el servidor");
            })                
        })
        return loginPromise;
    },
    getMenu(idPerfil) {
        var loginPromise = new Promise((resolve, reject) => {
            axios.get(server.host+'/SEG/listar/menu?idperfil='+idPerfil)
            .then(response => {
                // JSON responses are automatically parsed.
                if(response.data.data != null){
                    userStore.state.menu = response.data.data;
                    resolve(response.data);
                } else {
                    reject(response.data.message);
                }
            })
            .catch(e => {
                reject("Error al conectarse con el servidor");
            })                
        })
        return loginPromise;
    }
};