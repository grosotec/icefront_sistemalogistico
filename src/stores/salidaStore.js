import axios from 'axios';
import { server } from '../api/server';

export const salidaStore = {
    state: {
        selected: '',
        salidas: [],
        dsalidas: [],
        carrito: []
    },
    getSalidas() {
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host+'/LOG/Ventas')
            .then(response => {
                // JSON responses are automatically parsed.
                if(response.data.data != null){
                    salidaStore.state.salidas = [];
                    response.data.data.forEach(element => {
                        salidaStore.state.salidas.push(element);
                    });
                    resolve(response.data);
                } else {
                    reject("Error al obtener salidas");
                }
            })
            .catch(e => {
                reject("Error al conectarse con el servidor");
            })                
        })
        return promise;
    },
    getDetalleSalida(ccodventa) {
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host+'/LOG/DVentas?ccodventa='+ccodventa)
            .then(response => {
                // JSON responses are automatically parsed.
                if(response.data.data != null){
                    salidaStore.state.dsalidas = [];
                    response.data.data.forEach(element => {
                        salidaStore.state.dsalidas.push(element);
                    });
                    resolve(response.data);
                } else {
                    reject("Error al obtener dsalidas");
                }
            })
            .catch(e => {
                reject("Error al conectarse con el servidor");
            })                
        })
        return promise;
    },
    save(idcliente, ccodventa, base, igv, n_total, idusureg) {
        var savePromise = new Promise((resolve, reject) => {
                axios.post(server.host+'/LOG/Ventas/insert', {
                'idcliente' : idcliente,
                'ccodventa' : ccodventa,
                'ntotalparcial' : base,
                'nafecigv' : igv,
                'ntotal' : n_total,
                'idusureg' : idusureg
            })
            .then(response => {
                // JSON responses are automatically parsed.
                if (response.data.success == true) {
                    console.log("Venta guardada con exito");
                    resolve(response.data);
                } else {
                    //reject(response.data.message);
                    reject("Error al procesar el guardado");
                }
            })
            .catch(e => {
                reject("Error al conectarse con el servidor");
                console.log("Error: "+e);
            })
        })
        return savePromise;
    },
    saveDetalle(dventa) {
        var savePromise = new Promise((resolve, reject) => {
                axios.post(server.host+'/LOG/DVentas/insert', dventa)
            .then(response => {
                // JSON responses are automatically parsed.
                if (response.data.success == true) {
                    console.log("Venta guardada con exito");
                    resolve(response.data);
                } else {
                    //reject(response.data.message);
                    reject("Error al procesar el guardado");
                }
            })
            .catch(e => {
                reject("Error al conectarse con el servidor");
                console.log("Error: "+e);
            })
        })
        return savePromise;
    },
    /*
    edit(ccodproducto, cnombproducto, cdescproducto, 
        cdetalleproducto, idtipo, idcertificado,
        idunidmedida, idusumod, idproducto) {
             var editPromise = new Promise((resolve, reject) => {
                 axios.post(server.host+'/LOG/Productos/update', {
                    'ccodproducto' : ccodproducto,
                    'cnombproducto' : cnombproducto,
                    'cdescproducto' : cdescproducto,
                    'cdetalleproducto' : cdetalleproducto,
                    'idtipo' : idtipo,
                    'idcertificado' : idcertificado,
                    'idunidmedida' : idunidmedida,
                    'idusumod' : idusumod,
                    'idproducto' : idproducto
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Producto actualizado con exito");
                        productosStore.getProductos();
                        resolve(response.data);
                    } else {
                        //reject(response.data.message);
                        reject("Error al procesar el actualizado");
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                    console.log("Error: "+e);
                })
            })
            return editPromise;
    },
    baja(idproducto, idusumod) {
        var editPromise = new Promise((resolve, reject) => {
            axios.post(server.host+'/LOG/Productos/baja', {
                'idproducto': idproducto,
                'idusumod': idusumod
            })
           .then(response => {
               // JSON responses are automatically parsed.
               if (response.data.success == true) {
                   console.log("Personal bajado con exito");
                   productosStore.getProductos();
                   resolve(response.data);
               } else {
                   reject(response.data.message);
               }
           })
           .catch(e => {
               reject("Error al conectarse con el servidor");
               console.log("Error: "+e);
           })
       })
       return editPromise;
    }
    */
};