import axios from 'axios';
import { server } from '../api/server';

export const reporte_salidaStore = {
    state: {
        selected: '',
        reporte_salidas: [],
        uri: ''
    },
    getReporte_salida(fecha_inicio, fecha_final) {
        var promise = new Promise((resolve, reject) => {
            axios.get(server.host + '/LOG/KSalidaProductos?fechaini=' + fecha_inicio + '&fechafin=' + fecha_final)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        reporte_salidaStore.state.reporte_salidas = [];
                        response.data.data.forEach(element => {
                            reporte_salidaStore.state.reporte_salidas.push(element);
                        });
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })
        return promise;
    },
    getReporte(fecha_inicio, fecha_final) {
        var promise = new Promise(() => {
            // this.uri =  server.host + "/Reporte/Entradas?fechaini=" + this.reformatDateString(fecha_inicio) + "&fechafin=" + this.reformatDateString(fecha_final) + "&idalmacen=" + idalmacen;


            axios.get(server.host + '/Reporte/Salidas?fechaini=' + fecha_inicio + '&fechafin=' + fecha_final);

            reporte_salidaStore.state.uri = server.host + '/Reporte/Salidas?fechaini=' + fecha_inicio + '&fechafin=' + fecha_final;


        });
        return promise;
    },



};