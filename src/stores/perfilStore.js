import axios from 'axios';
import { server } from '../api/server';

export const perfilStore = {
    state: {

    },
    getPerfil() {
        var perfilPromise = new Promise((resolve, reject) => {
            axios.get(server.host + '/GEN/listar/perfiles')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data != null) {
                        resolve(response.data);
                    } else {
                        reject(response.data.message);
                    }
                })
                .catch(e => {
                    reject("Error al conectarse con el servidor");
                })
        })

        return perfilPromise;
    }
};